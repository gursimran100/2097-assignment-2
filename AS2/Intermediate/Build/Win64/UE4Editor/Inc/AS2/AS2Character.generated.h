// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AS2_AS2Character_generated_h
#error "AS2Character.generated.h already included, missing '#pragma once' in AS2Character.h"
#endif
#define AS2_AS2Character_generated_h

#define AS2_Source_AS2_AS2Character_h_18_RPC_WRAPPERS
#define AS2_Source_AS2_AS2Character_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define AS2_Source_AS2_AS2Character_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAS2Character(); \
	friend AS2_API class UClass* Z_Construct_UClass_AAS2Character(); \
public: \
	DECLARE_CLASS(AAS2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AS2"), NO_API) \
	DECLARE_SERIALIZER(AAS2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define AS2_Source_AS2_AS2Character_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAAS2Character(); \
	friend AS2_API class UClass* Z_Construct_UClass_AAS2Character(); \
public: \
	DECLARE_CLASS(AAS2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AS2"), NO_API) \
	DECLARE_SERIALIZER(AAS2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define AS2_Source_AS2_AS2Character_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAS2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAS2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAS2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAS2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAS2Character(AAS2Character&&); \
	NO_API AAS2Character(const AAS2Character&); \
public:


#define AS2_Source_AS2_AS2Character_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAS2Character(AAS2Character&&); \
	NO_API AAS2Character(const AAS2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAS2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAS2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAS2Character)


#define AS2_Source_AS2_AS2Character_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AAS2Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AAS2Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AAS2Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AAS2Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AAS2Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AAS2Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AAS2Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AAS2Character, L_MotionController); }


#define AS2_Source_AS2_AS2Character_h_15_PROLOG
#define AS2_Source_AS2_AS2Character_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AS2_Source_AS2_AS2Character_h_18_PRIVATE_PROPERTY_OFFSET \
	AS2_Source_AS2_AS2Character_h_18_RPC_WRAPPERS \
	AS2_Source_AS2_AS2Character_h_18_INCLASS \
	AS2_Source_AS2_AS2Character_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AS2_Source_AS2_AS2Character_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AS2_Source_AS2_AS2Character_h_18_PRIVATE_PROPERTY_OFFSET \
	AS2_Source_AS2_AS2Character_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	AS2_Source_AS2_AS2Character_h_18_INCLASS_NO_PURE_DECLS \
	AS2_Source_AS2_AS2Character_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AS2_Source_AS2_AS2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
