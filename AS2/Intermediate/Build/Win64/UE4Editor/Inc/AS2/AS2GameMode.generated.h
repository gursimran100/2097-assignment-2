// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AS2_AS2GameMode_generated_h
#error "AS2GameMode.generated.h already included, missing '#pragma once' in AS2GameMode.h"
#endif
#define AS2_AS2GameMode_generated_h

#define AS2_Source_AS2_AS2GameMode_h_12_RPC_WRAPPERS
#define AS2_Source_AS2_AS2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define AS2_Source_AS2_AS2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAS2GameMode(); \
	friend AS2_API class UClass* Z_Construct_UClass_AAS2GameMode(); \
public: \
	DECLARE_CLASS(AAS2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/AS2"), AS2_API) \
	DECLARE_SERIALIZER(AAS2GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define AS2_Source_AS2_AS2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAS2GameMode(); \
	friend AS2_API class UClass* Z_Construct_UClass_AAS2GameMode(); \
public: \
	DECLARE_CLASS(AAS2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/AS2"), AS2_API) \
	DECLARE_SERIALIZER(AAS2GameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define AS2_Source_AS2_AS2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AS2_API AAS2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAS2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AS2_API, AAS2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAS2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AS2_API AAS2GameMode(AAS2GameMode&&); \
	AS2_API AAS2GameMode(const AAS2GameMode&); \
public:


#define AS2_Source_AS2_AS2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AS2_API AAS2GameMode(AAS2GameMode&&); \
	AS2_API AAS2GameMode(const AAS2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AS2_API, AAS2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAS2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAS2GameMode)


#define AS2_Source_AS2_AS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define AS2_Source_AS2_AS2GameMode_h_9_PROLOG
#define AS2_Source_AS2_AS2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AS2_Source_AS2_AS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	AS2_Source_AS2_AS2GameMode_h_12_RPC_WRAPPERS \
	AS2_Source_AS2_AS2GameMode_h_12_INCLASS \
	AS2_Source_AS2_AS2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AS2_Source_AS2_AS2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AS2_Source_AS2_AS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	AS2_Source_AS2_AS2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AS2_Source_AS2_AS2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	AS2_Source_AS2_AS2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AS2_Source_AS2_AS2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
