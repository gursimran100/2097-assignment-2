// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define D(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Blue, TEXT(x));}


#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Pickup.generated.h"

/**
*
*/
UCLASS()
class AS2_API APickup : public AStaticMeshActor
{
	GENERATED_BODY()

		//trace stuff
protected:

	FString bName = "", bAction = "";
	bool bIsTraced = false;


public:
	UFUNCTION(BlueprintCallable, Category = "F")
		virtual void DoFunction() {};


public:

	//trace
	UFUNCTION(BlueprintPure, Category = "Traced")
		bool GetIsTraced() { return bIsTraced; }

	UFUNCTION(BlueprintCallable, Category = "Traced")
		void SetIsTraced(bool s) { bIsTraced = s; }

	//name
	UFUNCTION(BlueprintPure, Category = "Name")
		FString GetName() { return bName; }

	UFUNCTION(BlueprintCallable, Category = "Name")
		void SetName(FString s) { bName = s; }

	//action
	UFUNCTION(BlueprintPure, Category = "Action")
		FString GetAction() { return bAction; }

	UFUNCTION(BlueprintCallable, Category = "Action")
		void SetAction(FString s) { bAction = s; }


	//
	UFUNCTION(BlueprintNativeEvent, Category = "Pickup")
		void WasCollected();

	virtual void WasCollected_Implementation();


public:
	//default
	APickup();

	//
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//
	UFUNCTION(BlueprintPure, Category = "Pickup")
		bool IsActive();

	UFUNCTION(BlueprintCallable, Category = "Pickup")
		void SetActive(bool state);

protected:

	UPROPERTY(ReplicatedUsing = OnRep_IsActive)
		bool bIsActive;


	//when
	UFUNCTION()
		virtual void OnRep_IsActive();





};

