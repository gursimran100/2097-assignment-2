// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AS2GameMode.generated.h"

UCLASS(minimalapi)
class AAS2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAS2GameMode();
};



