// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AS2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AS2, "AS2" );
 