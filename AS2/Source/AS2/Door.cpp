// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "Components/StaticMeshComponent.h"

ADoor::ADoor() {

	//
	Super::SetName("Door");Super::SetAction("Press E to open");

	//keep movement synced from server to clients
	bReplicateMovement = true;

	//this is physics enabled and should move
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
	GetStaticMeshComponent()->SetSimulatePhysics(true);
}

void ADoor::DoFunction() {
	if (Role == ROLE_Authority) {

		//FOutputDeviceNull ar;

		//this->CallFunctionByNameWithArguments(TEXT("OpenDoor"), ar, NULL, true);
		D("opening door");
	}
}


void ADoor::WasCollected_Implementation() {
	Super::WasCollected_Implementation();

	Destroy();
}
