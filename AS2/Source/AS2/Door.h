// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define D(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT(x));}

#include "Engine.h"
#include "Pickup.h"
#include "Door.generated.h"

/**
*
*/
UCLASS()
class AS2_API ADoor : public APickup
{
	GENERATED_BODY()

public:
	virtual void DoFunction() override;



public:
	//default
	ADoor();

	//overridebase
	void WasCollected_Implementation() override;



protected:

	

};
