// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AS2GameMode.h"
#include "AS2HUD.h"
#include "AS2Character.h"
#include "UObject/ConstructorHelpers.h"

AAS2GameMode::AAS2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAS2HUD::StaticClass();
}
