// Fill out your copyright notice in the Description page of Project Settings.



#include "Pickup.h"
#include "AS2.h"
#include "Net/UnrealNetwork.h"



APickup::APickup() {
	//tells to replicate actor
	bReplicates = true;

	//don't need to tick
	PrimaryActorTick.bCanEverTick = false;

	if (Role == ROLE_Authority) {
		bIsActive = true;
	}
}

void APickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickup, bIsActive);

}

bool APickup::IsActive() { return bIsActive; }
void APickup::SetActive(bool state) {
	if (Role == ROLE_Authority) {
		bIsActive = state;
	}
}

void APickup::OnRep_IsActive() {
}

void APickup::WasCollected_Implementation() {
	///
	D("APickup::WasCollected_Implementation()");

}